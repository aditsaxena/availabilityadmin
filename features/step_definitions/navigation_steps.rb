When(/^I visit "(.*?)"$/) do |page|
  visit page
end

When(/^I fill in "(.*?)" on "(.*?)" field$/) do |value, field_name|
  fill_in field_name, :with => value
end

When(/^I click "(.*?)"$/) do |button_value|
  click_button button_value
end

When(/^I login in with email "(.*?)" and password "(.*?)"$/) do |email, password|
  fill_in 'user_email', :with => email
  fill_in 'user_password', :with => password
  click_button 'Login'
end

Then(/^should( not)? be on page "(.*?)"$/) do |should_not, content|
  if should_not
    expect(find('h2').text).not_to eq content
  else
    expect(find('h2').text).to eq content
  end
end

When(/^I query the "(.*?)" field with "(.*?)"$/) do |field_name, value|
  fill_in "q_#{field_name}", :with => value
  click_button 'Filter'
end

Then(/^I should see "(.*?)"$/) do |what_to_view|
  page.should have_content(what_to_view)
end

When(/^I click link "(.*?)"$/) do |what_to_click|
  click_link what_to_click
end

When /^I wait for (\d+) seconds?$/ do |n|
  sleep(n.to_i)
end

