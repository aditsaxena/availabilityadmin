Given(/^I remove availabilty for a week \((\d+\/\d+\/\d+) to (\d+\/\d+\/\d+)\)$/) do |date_start, date_end|
  browser_set_availability date_start, date_end, false
end

Then(/^I should see (not )?available for dates \((\d+\/\d+\/\d+) to (\d+\/\d+\/\d+)\)$/) do |should_not, date_start, date_end|
  assert_visibility(should_not, date_start, date_end)
end

Given(/^then add a day back in \((\d+\/\d+\/\d+)\)$/) do |sel_date|
  browser_set_availability sel_date, sel_date, true
end

Then(/^I should see (\d+) separate unavailable ranges$/) do |n_ranges|
  find_ranges_script = """$('.fc-day').map(function() { return $(this).hasClass('space-unavailable'); }).get().join('').split('false').filter(function(v){return v!==''}).length;"""
  
  res_n_ranges = page.evaluate_script(find_ranges_script)
  expect(res_n_ranges).to eq n_ranges.to_i
end


