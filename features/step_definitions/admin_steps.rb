Given(/^I am not authenticated$/) do
   # of course we're not logged in!
end

Given(/^I am an authenticated admin$/) do
  email = "demo@example.com"
  password = "demodemo"
  
  visit '/admin/login'
  fill_in "user_email", :with => email
  fill_in "user_password", :with => password
  click_button "Login"
  page.should have_content('Logout')
end

