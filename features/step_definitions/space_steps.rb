Then(/^I should see following column names "(.*?)"$/) do |column_names|
  column_names.split(', ').each do |column_name|
    page.should have_css('table th a', :text => column_name)
  end
end

