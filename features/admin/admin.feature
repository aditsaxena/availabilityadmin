Feature: Admin area
  In order to secure company's private functionalities
  As an admin
  I should be able to login into administrative area
  
  Scenario: Successful login
  
    Given I am not authenticated
    When I visit "/admin"
    And I login in with email "demo@example.com" and password "demodemo"
    Then should be on page "Dashboard"

  
  Scenario: Unsuccessful login
  
    Given I am not authenticated
    When I visit "/admin"
    And I login in with email "demo@example.com" and password "wo"
    Then should not be on page "Dashboard"

