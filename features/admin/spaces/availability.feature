@javascript
Feature: Space availability
  In order to judge the availability of a space quickly
  As an admin
  I want to be able to view a space’s available dates
  
  Background:
    Given I am an authenticated admin
    And I visit "/admin/spaces/dover-street-gallery/availability"


  Scenario: Alter a space's availability from the form
    And I remove availabilty for a week (3/2/2014 to 9/2/2014)
    Then I should see available for dates (1/2/2014 to 2/2/2014)
    Then I should see not available for dates (3/2/2014 to 9/2/2014)
    Then I should see available for dates (10/2/2014 to 28/2/2014)
  
  
  Scenario: Alter a space's availability from the form
    And I remove availabilty for a week (3/2/2014 to 9/2/2014)
    And then add a day back in (5/2/2014)
    Then I should see 2 separate unavailable ranges
