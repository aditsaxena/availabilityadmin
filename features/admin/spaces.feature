Feature: List and edit Spaces
  In order to manage lots of spaces
  As an admin
  I should be able list and edit spaces through filters
  
  
  Background:
    Given I am an authenticated admin
    And I visit "/admin/spaces"
  
  
  Scenario: List spaces
    Then I should see following column names "Id, Name, Slug, Description, Weekly Price Pence"

  
  Scenario: Filter spaces
    When I query the "name" field with "Kings Road"
    Then I should see "Displaying 1 Space"

  
  Scenario: Wrong Filter spaces
    When I query the "name" field with "non_really_anything"
    Then I should see "No Spaces found"
  
  
  Scenario: Paginate
    When I insert 30 of elements of type "space"
    And I visit "/admin/spaces"
    Then I should see "Next"
    And I should see "Last"

  
  Scenario: View a space
    When I query the "name" field with "Kings Road"
    When I click link "View"
    Then I should see "An immaculate retail unit located on the Kings Road, Chelsea."

  
  Scenario: View a space's availability page
    When I query the "name" field with "Kings Road"
    When I click link "Availability"
    Then I should see "Admin / Spaces / Kings Road / Availability"


