def browser_set_availability(date_start, date_end, set_available = true)
  str_date_start = Date.parse(date_start).strftime("%d-%m-%Y")
  str_date_end   = Date.parse(date_end).strftime("%d-%m-%Y")
  
  draggable = page.find_by_id("cellday-#{str_date_start}")
  droppable = page.find_by_id("cellday-#{str_date_end}")
  
  draggable.drag_to(droppable)
  
  if set_available
    click_button "Available"
  else
    click_button "Not Available"
  end
  
  sleep(2)
end

def assert_visibility(should_not, date_start, date_end)
  date_start = Date.parse(date_start)
  date_end   = Date.parse(date_end)
  
  (date_start..date_end).each do |sel_date|
    txt_sel_date = sel_date.strftime("%d-%m-%Y")
    cell_id = "#cellday-#{txt_sel_date}.space-unavailable"
    
    if should_not
      page.should have_css(cell_id)
    else
      page.should_not have_css(cell_id)
    end
  end
end

