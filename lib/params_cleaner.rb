class ParamsCleaner
  
  def self.as_date(date, date_class: Date)
    date_class.parse(date) rescue nil
  end
  
  def self.as_boolean(bool)
    bool and !! bool.to_s.match(/^(true|t|yes|y|1)$/i)
  end
  
end