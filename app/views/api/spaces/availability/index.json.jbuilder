json.array! @availability do |row|
  json.start row.sel_date if row.unavailable?
end
