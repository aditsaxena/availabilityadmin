ActiveAdmin.register Space do
  permit_params :name, :slug, :description, :weekly_price_pence

  index do
    selectable_column
    id_column
    column :name
    column :slug
    column :description
    column :weekly_price_pence
    
    column "Availability" do |space|
      link_to 'Availability', availability_admin_space_path(space)
    end
    
    actions
  end

  filter :name
  filter :slug
  filter :description
  filter :weekly_price_pence

  form do |f|
    f.inputs "Space Details" do
      f.input :name
      f.input :slug
      f.input :description
      f.input :weekly_price_pence
    end
    f.actions
  end
  
  member_action :availability, :method => :get do
    @space = Space.friendly.find(params[:id])
    @availability = Availability.new space: @space
  end
  
  controller do
    def find_resource
      scoped_collection.friendly.find(params[:id])
    end
  end
end
