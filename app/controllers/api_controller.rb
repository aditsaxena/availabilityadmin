class ApiController < ApplicationController
  
  respond_to :json
  before_filter :authenticate_user!
  
  
  protected
  
    def ajax_success(resdata)
      res = { :success => true, :data => resdata }
      res = params[:callback] ? "#{params[:callback]}("+res.to_json+');' : res.to_json
      
      respond_to do |format|
        format.html
        format.js
        format.json { render :json => res }
      end
    end
    
    def ajax_error(e, url_to = root_url)
      respond_to do |format|
        format.html { redirect_to url_to, :flash => { :error => e } }
        format.js
        format.json  { render :json => { :success => false, :message => e.to_s } }
      end
      true
    end
    
end
