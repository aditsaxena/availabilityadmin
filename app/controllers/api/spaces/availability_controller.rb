class Api::Spaces::AvailabilityController < ApiController
  
  before_action :set_service, only: [:index, :create]
  
  def index
    @availability = @avail_service.get_availability or return ajax_error(@avail_service.full_error)
  end
  
  def create
    if @avail_service.set_availability
      ajax_success t("availability.edited")
    else
      ajax_error(@avail_service.full_error)
    end
  end
  
  protected
    
    def set_service
      @avail_service = AvailabilityService.new params
    end
  
end
