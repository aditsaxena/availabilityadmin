jQuery ->
  $calendar = $("#calendar")
  space_id = $calendar.data('space-id')
  space_availability_url = "/api/spaces/#{ space_id }/availability.json"
  
  fix_date_end = (end) -> end.add('days', -1).format 'DD-MM-YYYY' # fault on fullcalendar js side
  cell_selector = (date) -> "cellday-#{date.format('DD-MM-YYYY')}"
  
  $calendar.fullCalendar
    defaultView: 'month'
    header:
      left: "prev,next today"
      center: "title"
      right: ""

    selectable: true
    selectHelper: true
    select: (start, end) ->
      $.popup_edit_availability
        space_id: space_id
        start: start.format 'DD-MM-YYYY'
        end: fix_date_end(end)
    
    loading: (bool) ->
      $('#calendar').fadeTo(0, if bool then 0.5 else 1)
    
    eventRender: (evt, element) ->
      $("#" + cell_selector(evt.start)).addClass('space-unavailable')
      true
    
    dayRender: (date, cell) ->
      cell.attr "id", cell_selector(date)
    
    eventSources: [
      space_availability_url
    ]
