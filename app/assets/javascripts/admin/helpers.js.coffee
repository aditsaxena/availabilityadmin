window.isempty = (el) -> el isnt ""

(($) ->
  uuid = 0
  $.fn.uniqueId = (prefix = 'ui-id') -> @each -> @id = "#{prefix}-#{++uuid}" unless @id
  
  $.fn.form_to_obj = ->
    array = this.serializeArray()
    json = {}
    $.each array, -> json[@name] = @value or ""
    json
  
  $.fn.dismiss_now = (callback = null) ->
    this.slideUp "normal", ->
      $(this).remove()
      callback() if callback
  
  $flash_cont = $("#flash_cont")
  $.flash_now = (msg, opts = {}) ->
    opts = $.extend {
      msg_type: 'notice'
      autodismiss: true
      dismiss_time: 2000
    }, opts
    
    curr_uid = uuid++
    flash_msgs = $('#flash_msgs')
    # console.log flash_msgs
    msg_html = """<div id="msg-#{ curr_uid }" class="msg alert-#{ opts.msg_type }"><div class="icon"></div><a class="gen_close_btn" data-dismiss="alert" href="#"></a><p>#{ msg }</p></div>"""
    if flash_msgs.length == 0
      $("""<div id="flash_msgs">#{ msg_html }</div>""").appendTo("#flash_cont")
    else
      $(msg_html).appendTo(flash_msgs)
    setTimeout ( -> $("#msg-#{ curr_uid }").dismiss_now() ), opts.dismiss_time if opts.autodismiss
  
  $flash_cont.on "click", ".gen_close_btn", ->
    $(this).parent().dismiss_now()
  
) jQuery

