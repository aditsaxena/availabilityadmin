jQuery ->
  $form_edit_availability = $('#form-edit-availability')
  $calendar = $("#calendar")
  $availability_form = $('#availability_search')
  $date_start = $('#date_start')
  $date_end = $('#date_end')

  $.popup_edit_availability = (params) ->
    set_action = (el, set_to_available) ->
      params = $.extend params, set_to_available: set_to_available
      set_availability params
      $(el).dialog("close")
    
    $form_edit_availability.dialog
      modal: true
      buttons:
        "Available": -> set_action(this, true)
        "Not Available": -> set_action(this, false)
    
  reset_form = ->
    $date_end.datepicker "option", "minDate", null
    $date_start.datepicker "option", "maxDate", null
    $availability_form[0].reset()
  
  reset_calendar = ->
    $calendar.find('.space-unavailable').removeClass('space-unavailable')
    $calendar.fullCalendar('refetchEvents')
    reset_form()
  
  window.set_availability = (params) ->
    $.ajax
      url: "/api/spaces/#{params.space_id}/availability/"
      dataType: "json"
      type: "POST"
      
      data:
        set_to_available: params.set_to_available
        start: params.start
        end:   params.end
      
      success: (res) ->
        if res.success
          $.flash_now(res.data)
          reset_calendar()
        else
          $.flash_now(res.message, msg_type: 'error')
          $availability_form.find("input").prop('disabled', false)

        
  $date_start.datepicker
    defaultDate: "+1w"
    dateFormat: 'dd-mm-yy'
    changeMonth: true
    changeYear: true
    onClose: (selectedDate) ->
      $date_end.datepicker "option", "minDate", selectedDate
  
  $date_end.datepicker
    defaultDate: "+1w"
    dateFormat: 'dd-mm-yy'
    changeMonth: true
    changeYear: true
    onClose: (selectedDate) ->
      $date_start.datepicker "option", "maxDate", selectedDate
  
  $availability_form.submit (e) ->
    set_availability $availability_form.form_to_obj()
    false
  
