#= require active_admin/base
#= require fullcalendar-2.0.0-beta2/lib/moment.min
#= require fullcalendar-2.0.0-beta2/fullcalendar/fullcalendar

#= require admin/helpers
#= require admin/spaces_availability_form
#= require admin/spaces_availability_calendar
