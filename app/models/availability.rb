class Availability < ActiveRecord::Base
  
  belongs_to :space
  
  validates_presence_of :space_id
  
  scope :from_recent, lambda { order(:sel_date) }
  scope :unavailable, lambda { where(unavailable: true) }
  scope :in_range, lambda { |date_start, date_end| where(sel_date: date_start..date_end) }
  
  def self.create_availabilities(space, date_start, date_end, set_to_available)
    space.availabilities.in_range(date_start, date_end).delete_all
    
    # sel_unavailabilities = []
    sel_unavailabilities = (date_start..date_end).map do |sel_date|
      new space: space, sel_date: sel_date, unavailable: ( ! set_to_available ) unless set_to_available
    end.compact
    
    import sel_unavailabilities
  end
  
end