class Space < ActiveRecord::Base
  
  extend FriendlyId
  friendly_id :name, use: :slugged
  
  has_many :availabilities
  
  def availability(date_start, date_end)
    self.availabilities.from_recent.unavailable.in_range(date_start, date_end)
  end
  
end
