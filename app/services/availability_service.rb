class AvailabilityService
  
  include ActiveModel::Model
  include Virtus.model

  attribute :space, Space
  attribute :date_start, Date
  attribute :date_end, Date
  attribute :set_to_available, Boolean
  
  validate :valid_space, :valid_range
  validates_presence_of :date_start, :date_end
  
  def initialize(params, params_cleaner: ParamsCleaner)
    @params = params
    @params_cleaner = params_cleaner
  end
  
  def get_availability
    return false unless valid?
    space.availability date_start, date_end
  end
  
  def set_availability
    return false unless valid?
    Availability.create_availabilities space, date_start, date_end, set_to_available?
    true
  end
  
  def full_error
    parse_error self.errors.messages
  end
  
  private
    
    def valid_space
      self.space = Space.friendly.find @params[:id] rescue self.errors.add(:space, "is not valid")
    end
    
    def valid_range
      self.date_start = @params_cleaner.as_date(@params[:start])
      self.date_end = @params_cleaner.as_date(@params[:end])
      self.errors.add(:date_end, "should be after or equal to date start") if date_start && date_end && date_end < date_start
    end
    
    def parse_error(errors)
      errors.map { |key, value| "#{key.to_s.humanize} #{ value.join(', ') }" }.join(', ')
    end

    def set_to_available?
      @params_cleaner.as_boolean(@params[:set_to_available])
    end
    
end
