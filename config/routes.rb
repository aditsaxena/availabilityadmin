AvailabilityAdmin::Application.routes.draw do
  
  get "home/index"
  root :to => 'home#index'
  # resource
  
  devise_for :users, ActiveAdmin::Devise.config
  
  ActiveAdmin.routes(self)
  
  namespace :api do
    
    resources :spaces, only: [], module: 'spaces' do
      member do
        resources :availability, only: [:index, :create]
      end
    end
    
  end
  
  
end
