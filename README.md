Availabilityadmin
=========

Build a `Rails application` to allow our admins to `privately` manage the availability of Spaces, admins should `be able to add and remove arbitrary date ranges` from a `spaces availability calendar`.

User stories for this sprint:

    In order to judge the availability of a space
    As an admin
    I want to be able to view a space's available dates

    In order to update the availability of a space
    As an admin
    I want to be able to add and remove the availability information on a space

The business also wants all calendars to be `available by default` and they need to be able to `add and remove arbitrary date ranges` so keep in mind things like the following scenario:

    I remove availabilty for a week (3/2/2014 to 9/2/2014)
    And then add a day back in (5/2/2014)
    I should see two separate unavailable ranges

Also, it’s important to remember that the `admins will be managing lots of spaces` so you should try an make your solution as `user friendly` as possible.


![Possible application layout](https://bytebucket.org/aditsaxena/availabilityadmin/raw/229d794dacd124aa1115752dac05e879f56d549c/layout.png)


#Online demo

You can access to the online demo:

- http://availabilityadmin.herokuapp.com/admin/login

Credentials:

- User: demo@example.com
- Password: demodemo


#Full stack development

Skills and technologies used:  

- Setup of a basic `API RESTFUL service`

- `Virtus` for class validation

- `activeadmin` for admin backend

- `Coffeescript`, `jQuery`, `Fullcalendar jQuery Plugin`

- `Heroku` deployment

- `Outside-in` Test Driven Development:
    - `RSpec` for unit test
    - `Cucumber` with `Capybara` for BDD integration
    - Use of `Factories` and `stubs`
    



