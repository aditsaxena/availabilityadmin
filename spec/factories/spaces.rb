# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :space do
    sequence :name do |n|
      "My Space #{n}"
    end
    
    description { ["My Space description", nil].sample }
    weekly_price_pence { [rand(200) * 2000, nil].sample }
  end
end
