# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :availability do
    belongs_to ""
    date_start "2014-02-13"
    date_end "2014-02-13"
    available false
  end
end
