require 'spec_helper'

describe Availability do
  
  def current_availability
    @space.availability(@date_start, @date_end)
  end
  
  def set_availability(new_value, space: @space, date_start: @date_start, date_end: @date_end)
    Availability.create_availabilities space, date_start, date_end, new_value
  end
  
  let(:good_space) { FactoryGirl.create(:space) }
  
  before do
    @space      = FactoryGirl.create(:space)
    @date_start = 10.days.ago.to_date
    @date_end   = @date_start + 4.days
  end
  
  it "inserts the availability of a space" do
    expect(Availability.count).to eq 0
    set_availability false
    expect(current_availability.length).to eq 5
  end
  
  it "list empty availability for empty data" do
    expect(Availability.count).to eq 0
    expect(current_availability.length).to eq 0
  end
  
  it "generates different availability for different spaces" do
    set_availability false, space: good_space
    expect(current_availability.length).to eq 0
    expect(Availability.count).to eq 5
    
    set_availability false, space: @space
    expect(current_availability.length).to eq 5
    
    expect(Availability.count).to eq 5 + 5
  end
  
  it "substracts the availability of a space" do
    set_availability false
    set_availability true, date_start: 8.days.ago.to_date, date_end: 7.days.ago.to_date
    expect(current_availability.length).to eq 3
  end
  
  it "substracts the availability of a space even if overlapping" do
    set_availability false
    set_availability true, date_start: 8.days.ago.to_date, date_end: 3.days.ago.to_date
    expect(current_availability.length).to eq 2
  end
  
  it "doesn't raise error when updating multiple times" do
    set_availability false
    set_availability false
    expect(Availability.count).to eq 5
    set_availability true
    expect(Availability.count).to eq 0
  end
  
end
