require 'spec_helper'

describe Space do
  
  let(:space) { Space.new name: "My beautiful space" }
  
  it "loads seeds" do
    expect(Space.count).to be > 0
  end
  
  it "has slug functionality" do
    expect(space).to respond_to :slug
    expect(space).to be_valid
    expect(space.slug).to eq 'my-beautiful-space'
  end
  
end
