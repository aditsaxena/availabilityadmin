require 'spec_helper'

describe AvailabilityService do
  
  let(:space)            { double }
  let(:valid_space_id)   { Space.stub_chain(:friendly, :find).and_return(space) ; "valid-space-id" }
  let(:invalid_space_id) { Space.stub_chain(:friendly, :find).and_raise("not found") ; "invalid-space-id" }
  
  let(:valid_params) do
    {
      space_id: valid_space_id,
      start: "5-05-2000",
      end: "20-05-2000",
    }
  end
  
  let(:valid_params_set) do
    valid_params.merge(set_to_available: true)
  end
  
  context "validation" do
    
    it "validates for valid input" do
      service = AvailabilityService.new valid_params
      
      expect(service.full_error).to be_blank
      expect(service.valid?).to eq true
      expect(service.full_error).to be_blank
    end
    
    it "returns error for missing start" do
      valid_params[:start] = nil
      service = AvailabilityService.new valid_params
      
      expect(service.valid?).to eq false
    end
    
    it "returns error for missing end" do
      valid_params[:end] = nil
      service = AvailabilityService.new valid_params
      
      expect(service.valid?).to eq false
    end
    
    it "returns error for invalid range" do
      valid_params[:start], valid_params[:end] = valid_params[:end], valid_params[:start]
      service = AvailabilityService.new valid_params
      
      expect(service.valid?).to eq false
    end
    
    it "returns error for invalid space" do
      params = {
        space_id: invalid_space_id,
        start: "5-05-2000",
        end: "20-05-2000",
      }
      service = AvailabilityService.new params
      
      expect(service.valid?).to eq false
    end
    
  end
  
  context "get_availability" do
    
    it "returns availability for valid input" do
      service = AvailabilityService.new valid_params
      
      space.should_receive :availability
      expect(service.get_availability).not_to eq false
    end
    
    it "returns false availability for invalid input" do
      valid_params[:end] = nil
      service = AvailabilityService.new valid_params
      
      space.should_not_receive :availability
      expect(service.get_availability).to eq false
    end
    
  end
  
  context "set_availability" do
    
    it "sets availability for valid input" do
      service = AvailabilityService.new valid_params_set

      Availability.should_receive :create_availabilities      
      expect(service.set_availability).not_to eq false
    end
    
    it "returns false for invalid input" do
      valid_params_set[:end] = nil
      service = AvailabilityService.new valid_params_set
      
      space.should_not_receive :availability
      expect(service.set_availability).to eq false
    end
    
  end
  
end
