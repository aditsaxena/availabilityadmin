class CreateSpaces < ActiveRecord::Migration
  def change
    create_table :spaces do |t|
      t.string :name
      t.string :slug
      t.text :description
      t.integer :weekly_price_pence

      t.timestamps
    end
  end
end
