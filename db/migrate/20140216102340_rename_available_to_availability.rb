class RenameAvailableToAvailability < ActiveRecord::Migration
  def change
    rename_column :availabilities, :available, :unavailable
  end
end
