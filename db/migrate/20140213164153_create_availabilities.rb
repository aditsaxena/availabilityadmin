class CreateAvailabilities < ActiveRecord::Migration
  def change
    create_table :availabilities do |t|
      t.references :space
      t.date :sel_date, default: nil
      t.boolean :available, default: false

      t.timestamps
    end
    
    add_index :availabilities, [:space_id, :sel_date], unique: true

  end
end
