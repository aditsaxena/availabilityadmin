# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

default_admin = User.create email: 'demo@example.com', password: 'demodemo'

Space.create name: "Dover Street Gallery",
  slug: "dover-street-gallery",
  description: "Below Wolf and Badger Dover Street, this gallery retail unit is clean
    and can be designed to fit any brief.",
  weekly_price_pence: 411800

Space.create name: "Kings Road",
  slug: "kings-road",
  description: "An immaculate retail unit located on the Kings Road, Chelsea.",
  weekly_price_pence: 176500

Space.create name: "Shoreditch Gallery Space",
  slug: "shoreditch-gallery-space",
  description: "",
  weekly_price_pence: 117600

Space.create name: "'Wilkes Street Warehouse '",
  slug: "wilkes-street-warehouse",
  description: "",
  weekly_price_pence: 176500

Space.create name: "Shoreditch Townhouse",
  slug: "shoreditch-townhouse",
  description: "A stones throw from Shoreditch High Street is this 4 storey townhouse
    with rooftop over the surrounding areas",
  weekly_price_pence: 300000
